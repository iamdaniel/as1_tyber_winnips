import java.util.ArrayList;
import java.util.List;

import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class BooksGUI {

	

	private final List<BooksObject> booksList;
	private Stage stage;
	
	private Text author;
	private ImageView imageView;
	private Text pages;

	protected ListView<String> listView;
	protected TextField pageAdjustment;
	protected Text adjustmentText;
	protected Button setPages;

	// creating the GUI class with stage and model
	
	public BooksGUI(Stage stage, List<BooksObject> model) {
		this.stage = stage;
		this.booksList = model;
		
		//creating the button to change the amount of pages 
		
		setPages = new Button ("Set new page number");
		
		//display of author and page numbers 
		
		author = new Text();
		pages = new Text("Pages: ");
		adjustmentText = new Text();
		pageAdjustment = new TextField();
		
		// creation of a grid pane with the displayed objects and their location in the pane 
		
		GridPane gridPane= new GridPane();
		gridPane.setHgap(7.5);
		gridPane.setVgap(5);
		
		gridPane.add(author,0,0,3,1);
		gridPane.add(pages,0,1,1,1);
		gridPane.add(pageAdjustment,1,1,1,1);
		gridPane.add(setPages,2,1,1,1);
		gridPane.add(adjustmentText,0,2,3,3);
		
		gridPane.setPrefSize(100, 100);
		gridPane.setMaxHeight(100);
		gridPane.setMinHeight(100);
		gridPane.setAlignment(Pos.CENTER);
		
		ArrayList<String> titlesOfBooks = new ArrayList<>(booksList.size());
		
		for (BooksObject p : booksList) {
			titlesOfBooks.add(p.getTitle());
		}

		// display of the covers of the books by linking the image files 
		
		imageView = new ImageView(new Image(booksList.get(0).getCover()));
		pageAdjustment.setText(booksList.get(0).getPages() + "");
		author.setText("Author:  " + booksList.get(0).getAuthor());
		
		StackPane stackPane = new StackPane(imageView);
		stackPane.setMinWidth(500);
		
		imageView.setPreserveRatio(true);
		imageView.fitWidthProperty().bind(stackPane.widthProperty());
		
		ObservableList<String> list = FXCollections.observableArrayList();
		list.addAll(titlesOfBooks);
		
		// showing the list concerning the book data in the grid pane 
		
		listView = new ListView<String>(list);
		listView.setMinWidth(100);
		listView.getSelectionModel().select(0);
		listView.getSelectionModel().selectedItemProperty().addListener(this::selList);
	
		SplitPane root = new SplitPane();
		root.setDividerPositions(0.2);
		root.getItems().addAll(listView, gridPane);
			
		//defining the split of the pane as vertical 
		
		SplitPane sp = new SplitPane();
		sp.setOrientation(Orientation.VERTICAL);
		 
		//setting the division of the split pane in the middle of the scene (0.5)
		
		sp.setDividerPositions(0.5);
		sp.getItems().addAll(root, stackPane);
			 
		//setting a scene title, hopefully the viewer also thinks the books are nice :)
		
		Scene scene = new Scene(sp,1200,800);
		stage.setTitle("Some nice books.");
		stage.setScene(scene);
		stage.setMinHeight(400);
		stage.setMinWidth(700);
	}


	// start of the "show" 

	public void start() {
		stage.show();
	}

	public void selList(ObservableValue<? extends String> val, String oldValue, String newValue)
	{
		int index = listView.getSelectionModel().getSelectedIndex();
		
		imageView.setImage(new Image(booksList.get(index).getCover()));
		pageAdjustment.setText(booksList.get(index).getPages() + "");
		author.setText("Author:  " + booksList.get(index).getAuthor());
		adjustmentText.setText("");
	}
}
