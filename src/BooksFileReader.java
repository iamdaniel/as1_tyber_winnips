import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class BooksFileReader {


    public ArrayList<BooksObject> readBooksCSV() {

        File file = new File ("src\\Books.csv");
        ArrayList<BooksObject> booksList = new ArrayList<BooksObject>();

        // open a new scanner called scan1 which reads the file
        
        try {
            Scanner scan1 = new Scanner(file);

            while (scan1.hasNext()) {
                String input = scan1.nextLine();

        // input trimmer for the data from the csv file, data gets parsed into tempBook
                
                input = input.trim();
                if (input.length()>0) {
                    String[] values = input.split(";");
                    System.out.println(Arrays.toString(values));
                    BooksObject tempBook = new BooksObject(values[0],values[1],Integer.parseInt(values[2]),(values[3]));
                    booksList.add(tempBook);

                }
            }
            scan1.close();
        }

        //catching event from the scanner 
        
        catch (Exception e) {
            System.out.println(e.getMessage());

        }

        System.out.println(booksList);

        return booksList;
    }
}
