
public class BooksObject {

	private String title, author, cover;
	private int pages;
	

	// book constructor with variables title, author, pages and the cover

	public BooksObject(String title, String author, int pages, String cover) {
		this.title = title;
		this.author = author;
		this.pages = pages;
		this.cover = cover;
	}

	// getters & setters for all variables as well as a tostring method

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public int getPages() {
		return pages;
	}

	public void setPages(int pages) {
		this.pages = pages;
	}

	public String getCover() {
		return cover;
	}

	public void setCover(String cover) {
		this.cover = cover;
	}

	@Override
	public String toString() {
		return "Book [title=" + title + ", author=" + author + ", pages=" + pages + ", cover=" + cover + "]";
	}

}
