import java.util.List;

import javafx.application.Application;
import javafx.stage.Stage;

public class BooksMVC extends Application {

	// instantiation of controller, view and model 

	private BooksGUI view;
	private BooksController controller;
	private List<BooksObject> model;

	// main method with the launcher including start method to launch JavaFX

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		model = new BooksFileReader().readBooksCSV();
		view = new BooksGUI(stage, model);
		controller = new BooksController(model, view);

		view.start();
	}
}
