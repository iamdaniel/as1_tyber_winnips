import javafx.scene.paint.Color;
import java.util.List;

public class BooksController {

	//declaration of model & view as final 
	
	private final List<BooksObject> model;
	private final BooksGUI view;

	// instantion of controller linked to the list of books as well as the JavaFX GUI view

	public BooksController(List<BooksObject> model, BooksGUI view) {
		this.model = model;
		this.view = view;
		
		
	// some editions have different amount of pages, this can be changed in the data
		
		view.setPages.setOnAction((event) -> {
			try {
				int newPages = Integer.parseInt(view.pageAdjustment.getText());
				model.get(view.listView.getSelectionModel().getSelectedIndex()).setPages(newPages);
				view.adjustmentText.setText("This edition has: " + newPages + " pages");
				view.adjustmentText.setFill(Color.GREEN);
				System.out.println("This edition has: " + newPages + " pages");
			} catch(NumberFormatException e) {
				view.adjustmentText.setText("please correct number of pages");
				view.adjustmentText.setFill(Color.RED);
				System.out.println("please correct number of pages");
			} catch (Exception e) {
				view.adjustmentText.setText(e.getMessage());
				System.out.println(e.getMessage());
			}
		});
	}
}
